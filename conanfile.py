from conans import ConanFile, tools


class StandardPathsConan(ConanFile):
    name = "standard_paths"
    version = "0.1.1"
    license = "MIT"
    author = "Kai Dohmen psykai1993@googlemail.com"
    url = "https://gitlab.com/Psy-Kai/standard_paths"
    description = "Helper library to query paths to place files"
    settings = "os", "compiler"
    exports_sources = "src/**.h"
    no_copy_source = True

    def build_requirements(self):
        self.build_requires("gtest/1.10.0", force_host_context=True);

    def package(self):
        self.copy("*.h", dst="include", src="src")

    def package_id(self):
        self.info.header_only()

    def package_info(self):
        self.cpp_info.system_libs = []
        compilerVersion = tools.Version(self.settings.compiler.version)

        if self.settings.compiler == "clang":
            if compilerVersion.major < "9":
                self.cpp_info.system_libs.append("c++fs")
            elif compilerVersion.major < "7":
                self.cpp_info.system_libs.append("c++experimental")
        elif self.settings.compiler == "gcc":
            if compilerVersion < "9.1":
                self.cpp_info.system_libs.append("stdc++fs")

        if self.settings.os == "Windows":
            self.cpp_info.system_libs.extend("ole32", "Shell32")
