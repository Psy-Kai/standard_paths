import qbs.FileInfo

Project {
    Product {
        name: "standard_paths"

        files: [
            "standard_paths/standard_paths.h",
            "standard_paths/standard_paths_unix.h",
            "standard_paths/standard_paths_win.h",
        ]

        cpp.cxxLanguageVersion: "c++17"
        cpp.minimumWindowsVersion: "6.1"
        cpp.includePaths: sourceDirectory

        Group {
            fileTagsFilter: "hpp"
            qbs.install: true
            qbs.installSourceBase: sourceDirectory
            qbs.installDir: "include"
        }

        Depends {
            name: "cpp"
        }
        Depends {
            name: "Exporter"
            submodules: [
                "qbs",
            ]
        }
        Export {
            cpp.includePaths: product.sourceDirectory
            prefixMapping: [
                {
                    prefix: product.sourceDirectory,
                    replacement:  FileInfo.joinPaths(qbs.installRoot, qbs.installPrefix, "include")
                }
            ]
            cpp.cxxLanguageVersion: "c++17"
            cpp.minimumWindowsVersion: "6.1"
            cpp.staticLibraries:  {
                var staticLibraries = [];

                if (qbs.toolchain.contains("clang")) {
                    if (cpp.compilerVersionMajor < 9)
                        staticLibraries.push("c++fs");
                    else if (cpp.compilerVersionMajor < 7)
                        staticLibraries.push("c++experimental");
                } else if (qbs.toolchain.contains("gcc")) {
                    if (cpp.compilerVersionMajor < 9)
                        staticLibraries.push("stdc++fs");
                    else if ((cpp.compilerVersionMajor === 9) && (cpp.compilerVersionMinor < 1))
                        staticLibraries.push("stdc++fs");
                } else if (qbs.toolchain.contains("msvc")) {
                    staticLibraries.push("Shell32")
                }

                if (qbs.targetOS.contains("windows"))
                    staticLibraries.push("ole32");
                return staticLibraries;
            }

            Depends {
                name: "cpp"
            }
        }
    }
}
