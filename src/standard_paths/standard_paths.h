#pragma once
#ifndef STANDARD_PATHS_STANDARD_PATHS_H
#define STANDARD_PATHS_STANDARD_PATHS_H

#include <cassert>

#if defined(_WIN32)
#include "standard_paths_win.h"
#elif defined(__ANDROID__)

namespace standard_paths {
namespace impl_android {} // namespace impl_android
namespace impl = impl_android;
} // namespace standard_paths

#elif defined(__unix)
#include "standard_paths_unix.h"
#else
static_assert(false, "Unknown plattform");
#endif

namespace standard_paths {

class App final {
public:
    explicit App(std::string_view name) noexcept : m_name{name}
    {
        assert(App::instance() == nullptr);
        App::instance() = this;
    }
    ~App()
    {
        App::instance() = nullptr;
    }
    static std::string_view name() noexcept
    {
        assert(App::instance() != nullptr);
        return App::instance()->m_name;
    }

private:
    static App *&instance()
    {
        static App *instance = nullptr;
        return instance;
    }
    std::string_view m_name;
};

inline Filesystem::path home()
{
    return impl::home();
}

inline Filesystem::path data()
{
    return impl::data();
}

inline Filesystem::path config()
{
    return impl::config();
}

inline Filesystem::path cache()
{
    return impl::cache();
}

inline Filesystem::path runtime()
{
    return impl::runtime();
}

inline std::vector<Filesystem::path> dataDirs()
{
    return impl::dataDirs();
}

inline std::vector<Filesystem::path> configDirs()
{
    return impl::configDirs();
}

inline Filesystem::path appData()
{
    return impl::data() / App::name();
}

inline Filesystem::path appConfig()
{
    return impl::config() / App::name();
}

inline Filesystem::path appCache()
{
    return impl::cache() / App::name();
}

inline Filesystem::path appRuntime()
{
    return impl::runtime() / App::name();
}

inline Filesystem::path desktop()
{
    return impl::desktop();
}

inline Filesystem::path download()
{
    return impl::download();
}

inline Filesystem::path templates()
{
    return impl::templates();
}

inline Filesystem::path publicshare()
{
    return impl::publicshare();
}

inline Filesystem::path documents()
{
    return impl::documents();
}

inline Filesystem::path music()
{
    return impl::music();
}

inline Filesystem::path pictures()
{
    return impl::pictures();
}

inline Filesystem::path videos()
{
    return impl::videos();
}

} // namespace standard_paths

#endif // STANDARD_PATHS_STANDARD_PATHS_H
