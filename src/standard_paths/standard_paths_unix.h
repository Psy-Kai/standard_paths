#pragma once
#ifndef STANDARD_PATHS_STANDARD_PATHS_UNIX_H
#define STANDARD_PATHS_STANDARD_PATHS_UNIX_H
#if defined(__unix)

#include <fstream>
#include <iostream>
#include <vector>
#include <unistd.h>

#if defined(__GNUC__)
#if (__GNUC__ < 8)
#define STANDARD_PATHS_USE_EXPERIMENTAL_FILESYSTEM_LIBRARY
#endif // __GNUC__ < 8
#elif defined(__clang__)
#if (__clang_major__ < 7)
#define STANDARD_PATHS_USE_EXPERIMENTAL_FILESYSTEM_LIBRARY
#endif // __clang_major__ < 7
#else
static_assert(false, "Unknown compiler");
#endif // filesystem check

#if defined(STANDARD_PATHS_USE_EXPERIMENTAL_FILESYSTEM_LIBRARY)
#include <experimental/filesystem>
#else
#include <filesystem>
#endif // STANDARD_PATHS_USE_EXPERIMENTAL_FILESYSTEM_LIBRARY

namespace standard_paths {

#if defined(STANDARD_PATHS_USE_EXPERIMENTAL_FILESYSTEM_LIBRARY)
namespace Filesystem = std::experimental::filesystem;
#else
namespace Filesystem = std::filesystem;
#endif // STANDARD_PATHS_USE_EXPERIMENTAL_FILESYSTEM_LIBRARY

namespace impl_unix {
namespace enviornment {
constexpr auto home = "HOME";
constexpr auto xdg_data_home = "XDG_DATA_HOME";
constexpr auto xdg_config_home = "XDG_CONFIG_HOME";
constexpr auto xdg_cache_home = "XDG_CACHE_HOME";
constexpr auto xdg_runtime_dir = "XDG_RUNTIME_DIR";
constexpr auto xdg_data_dirs = "XDG_DATA_DIRS";
constexpr auto xdg_config_dirs = "XDG_CONFIG_DIRS";
} // namespace enviornment
namespace userdirs {
constexpr auto desktop = "XDG_DESKTOP_DIR";
constexpr auto download = "XDG_DOWNLOAD_DIR";
constexpr auto templates = "XDG_TEMPLATES_DIR";
constexpr auto publicshare = "XDG_PUBLICSHARE_DIR";
constexpr auto documents = "XDG_DOCUMENTS_DIR";
constexpr auto music = "XDG_MUSIC_DIR";
constexpr auto pictures = "XDG_PICTURES_DIR";
constexpr auto videos = "XDG_VIDEOS_DIR";
} // namespace userdirs
namespace fallback {
namespace xdg {
constexpr auto data = ".local/share";
constexpr auto config = ".config";
constexpr auto cache = ".cache";
constexpr auto dataDirs =
    std::array{std::string_view{"/usr/local/share"}, std::string_view{"/usr/share"}};
constexpr auto configDirs = std::array{std::string_view{"/etc/xdg"}};
} // namespace xdg
constexpr auto runtime_ = "runtime-";
} // namespace fallback

inline Filesystem::path home()
{
    return std::getenv(enviornment::home);
}

inline bool empty(const char *pathFromEnv)
{
    return (pathFromEnv == nullptr) || std::string_view{pathFromEnv}.empty();
}

namespace from_enviornment {
inline Filesystem::path data()
{
    if (const auto *pathFromEnv = std::getenv(enviornment::xdg_data_home); !empty(pathFromEnv))
        return pathFromEnv;
    return {};
}

inline Filesystem::path config()
{
    if (const auto *pathFromEnv = std::getenv(enviornment::xdg_config_home); !empty(pathFromEnv))
        return pathFromEnv;
    return {};
}

inline Filesystem::path cache()
{
    if (const auto *pathFromEnv = std::getenv(enviornment::xdg_cache_home); !empty(pathFromEnv))
        return pathFromEnv;
    return {};
}

inline Filesystem::path runtime()
{
    if (const auto *pathFromEnv = std::getenv(enviornment::xdg_runtime_dir); !empty(pathFromEnv))
        return pathFromEnv;
    return {};
}

inline std::vector<Filesystem::path> dirsFromEnv(std::string_view paths)
{
    if (paths.empty())
        return {};

    auto dirs = std::vector<Filesystem::path>{};
    constexpr auto seperator = ':';
    for (std::size_t begin = 0, end = paths.find_first_of(seperator, begin);
         end != std::string_view::npos;
         begin = end + 1, end = paths.find_first_of(seperator, begin))
        dirs.emplace_back(paths.substr(begin, end));
    return dirs;
}

inline std::vector<Filesystem::path> dataDirs()
{
    auto dataDirs = std::vector<Filesystem::path>{};
    if (const auto *pathsFromEnv = std::getenv(enviornment::xdg_data_dirs); !empty(pathsFromEnv))
        return dirsFromEnv(pathsFromEnv);
    return {};
}

inline std::vector<Filesystem::path> configDirs()
{
    auto dataDirs = std::vector<Filesystem::path>{};
    if (const auto *pathsFromEnv = std::getenv(enviornment::xdg_config_dirs); !empty(pathsFromEnv))
        return dirsFromEnv(pathsFromEnv);
    return {};
}
} // namespace from_enviornment

namespace fallback {
inline Filesystem::path data()
{
    return home() / fallback::xdg::data;
}

inline Filesystem::path config()
{
    return home() / fallback::xdg::config;
}

inline Filesystem::path cache()
{
    return home() / fallback::xdg::cache;
}

inline Filesystem::path runtime()
{
    const auto path = Filesystem::temp_directory_path() / (runtime_ + std::to_string(geteuid()));
    if (!Filesystem::exists(path)) {
        Filesystem::create_directories(path);
        std::clog << "XDG_RUNTIME_DIR not set, defaulting to " << path << '\n';
        Filesystem::permissions(path,
                                Filesystem::perms::owner_all | Filesystem::perms::set_uid |
                                    Filesystem::perms::set_gid);
    }
    return path;
}

inline std::vector<Filesystem::path> dataDirs()
{
    auto dataDirs = std::vector<Filesystem::path>{};
    for (const auto dir : fallback::xdg::dataDirs)
        dataDirs.emplace_back(dir);
    return dataDirs;
}

inline std::vector<Filesystem::path> configDirs()
{
    auto dataDirs = std::vector<Filesystem::path>{};
    for (const auto dir : fallback::xdg::configDirs)
        dataDirs.emplace_back(dir);
    return dataDirs;
}
} // namespace fallback

inline Filesystem::path data()
{
    if (const auto path = from_enviornment::data(); !path.empty())
        return path;
    return fallback::data();
}

inline Filesystem::path config()
{
    if (const auto path = from_enviornment::config(); !path.empty())
        return path;
    return fallback::config();
}

inline Filesystem::path cache()
{
    if (const auto path = from_enviornment::cache(); !path.empty())
        return path;
    return fallback::cache();
}

inline Filesystem::path runtime()
{
    if (const auto path = from_enviornment::runtime(); !path.empty())
        return path;
    return fallback::runtime();
}

inline std::vector<Filesystem::path> dataDirs()
{
    if (const auto dirs = from_enviornment::dataDirs(); !dirs.empty())
        return dirs;
    return fallback::dataDirs();
}

inline std::vector<Filesystem::path> configDirs()
{
    if (const auto dirs = from_enviornment::configDirs(); !dirs.empty())
        return dirs;
    return fallback::configDirs();
}

inline Filesystem::path dirFromUserDirs(std::string_view dir)
{
    constexpr auto userDirsFile = "user-dirs.dirs";
    constexpr auto endl = '\n';
    constexpr auto quot = '"';
    auto skipEmptyLines = [](std::istream &stream) {
        while (stream.peek() == endl)
            stream.get();
    };
    auto readLine = [](std::istream &stream) {
        const std::size_t pos = stream.tellg();
        std::size_t endlPos = pos;
        for (; !stream.eof() && (stream.peek() != endl); endlPos = stream.tellg())
            stream.seekg(endlPos + 1);
        auto line = std::string(endlPos - pos, 0x00);
        stream.seekg(pos);
        stream.read(line.data(), line.size());
        return line;
    };
    auto getValidLine = [skipEmptyLines, readLine, dir](std::istream &stream) {
        auto line = std::string{};
        for (; line.find(dir) == std::string::npos;
             skipEmptyLines(stream), line = readLine(stream)) {}
        return line;
    };
    auto pathFromLine = [dir](std::string_view line) {
        auto begin = std::begin(line) + dir.size() + 1;
        auto end = std::end(line);
        if (*begin == quot) {
            ++begin;
            --end;
        }
        return Filesystem::path{begin, end};
    };

    auto stream = std::ifstream{config() / userDirsFile};
    if (!stream.is_open())
        return {};

    const auto line = getValidLine(stream);
    if (line.empty())
        return {};
    return pathFromLine(line);
}

inline Filesystem::path desktop()
{
    return dirFromUserDirs(userdirs::desktop);
}

inline Filesystem::path download()
{
    return dirFromUserDirs(userdirs::download);
}

inline Filesystem::path templates()
{
    return dirFromUserDirs(userdirs::templates);
}

inline Filesystem::path publicshare()
{
    return dirFromUserDirs(userdirs::publicshare);
}

inline Filesystem::path documents()
{
    return dirFromUserDirs(userdirs::documents);
}

inline Filesystem::path music()
{
    return dirFromUserDirs(userdirs::music);
}

inline Filesystem::path pictures()
{
    return dirFromUserDirs(userdirs::pictures);
}

inline Filesystem::path videos()
{
    return dirFromUserDirs(userdirs::videos);
}

} // namespace impl_unix
namespace impl = impl_unix;
} // namespace standard_paths

#endif // __unix
#endif // STANDARD_PATHS_STANDARD_PATHS_UNIX_H
