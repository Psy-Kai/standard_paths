#pragma once
#ifndef STANDARD_PATHS_STANDARD_PATHS_WIN_H
#define STANDARD_PATHS_STANDARD_PATHS_WIN_H
#if defined(_WIN32)

/* need wrong ordering because of windows -.-*/
#include <initguid.h>

#include <knownfolders.h>

#include <shlobj.h>

#if defined(_MSC_VER)
#if (_MSC_VER < 1914)
#define STANDARD_PATHS_USE_EXPERIMENTAL_FILESYSTEM_LIBRARY
#endif // _MSV_VER < 1914
#elif defined(__MINGW32__)
#if (__MINGW32_VERSION_MAJOR < 8)
#define STANDARD_PATHS_USE_EXPERIMENTAL_FILESYSTEM_LIBRARY
#endif // __MINGW32_VERSION_MAJOR < 8
#elif defined(__MINGW64__)
#if (__MINGW64_VERSION_MAJOR < 8)
#define STANDARD_PATHS_USE_EXPERIMENTAL_FILESYSTEM_LIBRARY
#endif // __MINGW32_VERSION_MAJOR < 8#elif defined(__clang__)
#elif defined(__clang__)
#if (__clang_major__ < 7)
#define STANDARD_PATHS_USE_EXPERIMENTAL_FILESYSTEM_LIBRARY
#endif // __clang_major__ < 7
#else
static_assert(false, "Unknown compiler");
#endif // filesystem check

#if defined(STANDARD_PATHS_USE_EXPERIMENTAL_FILESYSTEM_LIBRARY)
#include <experimental/filesystem>
#else
#include <filesystem>
#endif // STANDARD_PATHS_USE_EXPERIMENTAL_FILESYSTEM_LIBRARY

namespace standard_paths {
#if defined(STANDARD_PATHS_USE_EXPERIMENTAL_FILESYSTEM_LIBRARY)
namespace Filesystem = std::experimental::filesystem;
#else
namespace Filesystem = std::filesystem;
#endif // STANDARD_PATHS_USE_EXPERIMENTAL_FILESYSTEM_LIBRARY

namespace impl_win {
inline Filesystem::path home()
{
    auto path = std::unique_ptr<wchar_t, decltype(&CoTaskMemFree)>{nullptr, &CoTaskMemFree};
    auto *ppszPath = path.get();
    SHGetKnownFolderPath(FOLDERID_Profile, KF_FLAG_DONT_VERIFY, 0, &ppszPath);
    return {ppszPath};
}

inline Filesystem::path appdataLocal()
{
    auto path = std::unique_ptr<wchar_t, decltype(&CoTaskMemFree)>{nullptr, &CoTaskMemFree};
    auto *ppszPath = path.get();
    SHGetKnownFolderPath(FOLDERID_LocalAppData, KF_FLAG_DONT_VERIFY, 0, &ppszPath);
    return {ppszPath};
}

inline Filesystem::path data()
{
    return appdataLocal();
}

inline Filesystem::path config()
{
    return appdataLocal();
}

inline Filesystem::path cache()
{
    constexpr auto cacheDir = "cache";
    return appdataLocal() / cacheDir;
}

inline Filesystem::path runtime()
{
    return home();
}

inline std::vector<Filesystem::path> dataDirs()
{
    return {appdataLocal()};
}

inline std::vector<Filesystem::path> configDirs()
{
    return {appdataLocal()};
}

inline Filesystem::path desktop()
{
    auto path = std::unique_ptr<wchar_t, decltype(&CoTaskMemFree)>{nullptr, &CoTaskMemFree};
    auto *ppszPath = path.get();
    SHGetKnownFolderPath(FOLDERID_Desktop, KF_FLAG_DONT_VERIFY, 0, &ppszPath);
    return {ppszPath};
}

inline Filesystem::path download()
{
    auto path = std::unique_ptr<wchar_t, decltype(&CoTaskMemFree)>{nullptr, &CoTaskMemFree};
    auto *ppszPath = path.get();
    SHGetKnownFolderPath(FOLDERID_Downloads, KF_FLAG_DONT_VERIFY, 0, &ppszPath);
    return {ppszPath};
}

inline Filesystem::path templates()
{
    return {};
}

inline Filesystem::path publicshare()
{
    auto path = std::unique_ptr<wchar_t, decltype(&CoTaskMemFree)>{nullptr, &CoTaskMemFree};
    auto *ppszPath = path.get();
    SHGetKnownFolderPath(FOLDERID_Public, KF_FLAG_DONT_VERIFY, 0, &ppszPath);
    return {ppszPath};
}

inline Filesystem::path documents()
{
    auto path = std::unique_ptr<wchar_t, decltype(&CoTaskMemFree)>{nullptr, &CoTaskMemFree};
    auto *ppszPath = path.get();
    SHGetKnownFolderPath(FOLDERID_Documents, KF_FLAG_DONT_VERIFY, 0, &ppszPath);
    return {ppszPath};
}

inline Filesystem::path music()
{
    auto path = std::unique_ptr<wchar_t, decltype(&CoTaskMemFree)>{nullptr, &CoTaskMemFree};
    auto *ppszPath = path.get();
    SHGetKnownFolderPath(FOLDERID_Music, KF_FLAG_DONT_VERIFY, 0, &ppszPath);
    return {ppszPath};
}

inline Filesystem::path pictures()
{
    auto path = std::unique_ptr<wchar_t, decltype(&CoTaskMemFree)>{nullptr, &CoTaskMemFree};
    auto *ppszPath = path.get();
    SHGetKnownFolderPath(FOLDERID_Pictures, KF_FLAG_DONT_VERIFY, 0, &ppszPath);
    return {ppszPath};
}

inline Filesystem::path videos()
{
    auto path = std::unique_ptr<wchar_t, decltype(&CoTaskMemFree)>{nullptr, &CoTaskMemFree};
    auto *ppszPath = path.get();
    SHGetKnownFolderPath(FOLDERID_Videos, KF_FLAG_DONT_VERIFY, 0, &ppszPath);
    return {ppszPath};
}
} // namespace impl_win
namespace impl = impl_win;
} // namespace standard_paths

#endif // _WIN32
#endif // STANDARD_PATHS_STANDARD_PATHS_WIN_H
