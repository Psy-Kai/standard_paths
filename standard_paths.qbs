import qbs.FileInfo
import qbs.Probes

Project {
    references: [
        "src",
        "tests",
        "test_package",
        FileInfo.joinPaths(conan.generatedFilesPath, "conanbuildinfo.qbs"),
    ]

    Product {
        name: "misc"
        condition: false

        files: [
            ".gitlab-ci.yml",
            "build.py",
            "conanfile.py",
            "LICENSE",
        ]
    }

    Probes.ConanfileProbe {
        id: conan
        conanfilePath: FileInfo.joinPaths(project.sourceDirectory, "conanfile.py")
        additionalArguments: ["--build=missing"]
        generators: "qbs"
        settings: {
            var settings = new Object;
            if (qbs.toolchain.contains("msvc")) {
                if (qbs.buildVariant === "debug")
                    settings["build_type"] = "Debug";
                else
                    settings["build_type"] = "Release";
            }

            return settings;
        }
    }
}
