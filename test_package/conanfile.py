from conans import ConanFile,  tools


class TestPackageConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "qbs"

    def _qbs_args(self):
        return "-f {}/test_package.qbs " \
               "projects.test_package.test_build_folder:{}".format(
                    self.source_folder, self.build_folder)

    def build(self):
        self.run("qbs build "+self._qbs_args())

    def test(self):
        if not tools.cross_building(self.settings):
            self.run("qbs run "+self._qbs_args())
