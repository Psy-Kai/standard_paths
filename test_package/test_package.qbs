import qbs.FileInfo

Project {
    readonly property string test_build_folder: undefined

    Properties {
        condition: !!test_build_folder
        references: FileInfo.joinPaths(test_build_folder, "conanbuildinfo.qbs")
    }

    CppApplication {
        files: [
            "conanfile.py",
            "test_package.cpp",
        ]

        cpp.cxxLanguageVersion: "c++17"

        Depends {
            name: "standard_paths"
        }
    }
}
