Application {
    name: "standard_paths_test"
    condition: qbs.debugInformation && gtest.present

    type: base.concat("autotest")

    files: [
        "standard_paths/standardpathsinclude.cpp",
    ]

    cpp.cxxLanguageVersion: "c++17"

    Group {
        name: "unix files"
        condition: qbs.targetOS.contains("unix")
        files: [
            "standard_paths/unix_standardpathstest.cpp"
        ]
    }
    Group {
        name: "windows files"
        condition: qbs.targetOS.contains("windows")
        files: [
            "standard_paths/win_standardpathstest.cpp",
        ]
    }

    Properties {
        condition: !qbs.targetOS.contains("windows")
        cpp.driverFlags: base.concat("-pthread")
    }

    Depends {
        name: "cpp"
    }
    Depends {
        name: "gtest"
        required: false
    }
    Depends {
        name: "standard_paths"
    }
}
