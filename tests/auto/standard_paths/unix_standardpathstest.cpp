#include <fstream>

#include <gtest/gtest.h>
#include <standard_paths/standard_paths.h>

GTEST_TEST(standard_paths_test_unix, home)
{
    const auto path = standard_paths::home();
    ASSERT_TRUE(path.is_absolute());
    ASSERT_EQ(path, std::getenv("HOME"));
}

GTEST_TEST(standard_paths_test_unix, dataFromEnv)
{
    if (const auto *pathFromEnv = std::getenv("XDG_DATA_HOME")) {
        const auto path = standard_paths::impl_unix::from_enviornment::data();
        ASSERT_TRUE(path.is_absolute());
        ASSERT_EQ(path, pathFromEnv);
    } else {
        std::cerr << "XDG_DATA_HOME not defined, could not test\n";
    }
}

GTEST_TEST(standard_paths_test_unix, dataFallback)
{
    const auto path = standard_paths::impl_unix::fallback::data();
    ASSERT_TRUE(path.is_absolute());
    ASSERT_EQ(path, standard_paths::home() / ".local" / "share");
}

GTEST_TEST(standard_paths_test_unix, configFromEnv)
{
    if (const auto *pathFromEnv = std::getenv("XDG_CONFIG_HOME")) {
        const auto path = standard_paths::impl_unix::from_enviornment::config();
        ASSERT_TRUE(path.is_absolute());
        ASSERT_EQ(path, pathFromEnv);
    } else {
        std::cerr << "XDG_CONFIG_HOME not defined, could not test\n";
    }
}

GTEST_TEST(standard_paths_test_unix, configFallback)
{
    const auto path = standard_paths::impl_unix::fallback::config();
    ASSERT_TRUE(path.is_absolute());
    ASSERT_EQ(path, standard_paths::home() / ".config");
}

GTEST_TEST(standard_paths_test_unix, cacheFromEnv)
{
    if (const auto *pathFromEnv = std::getenv("XDG_CACHE_HOME")) {
        const auto path = standard_paths::impl_unix::from_enviornment::cache();
        ASSERT_TRUE(path.is_absolute());
        ASSERT_EQ(path, pathFromEnv);
    } else {
        std::cerr << "XDG_CACHE_HOME not defined, could not test\n";
    }
}

GTEST_TEST(standard_paths_test_unix, cacheFallback)
{
    const auto path = standard_paths::impl_unix::fallback::cache();
    ASSERT_TRUE(path.is_absolute());
    ASSERT_EQ(path, standard_paths::home() / ".cache");
}

GTEST_TEST(standard_paths_test_unix, runtime_dir)
{
    const auto runtimePath = standard_paths::runtime();

    ASSERT_TRUE(runtimePath.is_absolute());
    ASSERT_TRUE(standard_paths::Filesystem::exists(runtimePath));
    ASSERT_TRUE(standard_paths::Filesystem::is_directory(runtimePath));
    const auto fileStatus = standard_paths::Filesystem::status(runtimePath);
    ASSERT_EQ(fileStatus.permissions() & standard_paths::Filesystem::perms::owner_all,
              standard_paths::Filesystem::perms::owner_all);
}

GTEST_TEST(standard_paths_test_unix, dataDirsFromEnv)
{
    if (const auto *pathsFromEnv = std::getenv("XDG_DATA_DIRS")) {
        const auto dataDirs = standard_paths::impl_unix::from_enviornment::dataDirs();
        const auto dataDirsView = std::string_view{pathsFromEnv};
        for (std::size_t begin = 0, end = dataDirsView.find_first_of(':', begin);
             end != std::string_view::npos;
             begin = end + 1, end = dataDirsView.find_first_of(':', begin)) {
            const auto path = standard_paths::Filesystem::path{
                standard_paths::Filesystem::path{dataDirsView.substr(begin, end)}};
            ASSERT_TRUE(path.is_absolute());
            ASSERT_NE(std::find(std::begin(dataDirs), std::end(dataDirs), path),
                      std::end(dataDirs));
        }
    } else {
        std::cerr << "XDG_DATA_DIRS not defined, could not test\n";
    }
}

GTEST_TEST(standard_paths_test_unix, dataDirsFallback)
{
    const auto dataDirs = standard_paths::impl_unix::fallback::dataDirs();
    ASSERT_EQ(dataDirs.size(), std::size_t{2});
    auto path = standard_paths::Filesystem::path{"/usr/local/share"};
    ASSERT_TRUE(path.is_absolute());
    ASSERT_NE(std::find(std::begin(dataDirs), std::end(dataDirs), path), std::end(dataDirs));
    path = standard_paths::Filesystem::path{"/usr/share"};
    ASSERT_TRUE(path.is_absolute());
    ASSERT_NE(std::find(std::begin(dataDirs), std::end(dataDirs), path), std::end(dataDirs));
}

GTEST_TEST(standard_paths_test_unix, configDirsFromEnv)
{
    if (const auto *pathsFromEnv = std::getenv("XDG_CONFIG_DIRS")) {
        const auto dataDirs = standard_paths::impl_unix::from_enviornment::configDirs();
        ASSERT_FALSE(dataDirs.empty());
        const auto dataDirsView = std::string_view{pathsFromEnv};
        for (std::size_t begin = 0, end = dataDirsView.find_first_of(':', begin);
             end != std::string_view::npos;
             begin = end + 1, end = dataDirsView.find_first_of(':', begin)) {
            const auto path = standard_paths::Filesystem::path{
                standard_paths::Filesystem::path{dataDirsView.substr(begin, end)}};
            ASSERT_TRUE(path.is_absolute());
            ASSERT_NE(std::find(std::begin(dataDirs), std::end(dataDirs), path),
                      std::end(dataDirs));
        }
    } else {
        std::cerr << "XDG_CONFIG_DIRS not defined, could not test\n";
    }
}

GTEST_TEST(standard_paths_test_unix, configDirsFallback)
{
    const auto configDirs = standard_paths::impl_unix::fallback::configDirs();
    ASSERT_FALSE(configDirs.empty());
    ASSERT_EQ(configDirs.size(), std::size_t{1});
    auto path = standard_paths::Filesystem::path{"/etc/xdg"};
    ASSERT_TRUE(path.is_absolute());
    ASSERT_NE(std::find(std::begin(configDirs), std::end(configDirs), path), std::end(configDirs));
}

GTEST_TEST(standard_paths_test_unix, app)
{
    const auto name = std::string_view{"some name"};
    const auto app = standard_paths::App{name};
    ASSERT_EQ(standard_paths::App::name(), name);
}

GTEST_TEST(standard_paths_test_unix, appData)
{
    const auto name = std::string_view{"app data"};
    const auto app = standard_paths::App{name};
    ASSERT_EQ(standard_paths::appData(), standard_paths::data() / name);
}

GTEST_TEST(standard_paths_test_unix, appConfig)
{
    const auto name = std::string_view{"app config"};
    const auto app = standard_paths::App{name};
    ASSERT_EQ(standard_paths::appConfig(), standard_paths::config() / name);
}

GTEST_TEST(standard_paths_test_unix, appCache)
{
    const auto name = std::string_view{"app cache"};
    const auto app = standard_paths::App{name};
    ASSERT_EQ(standard_paths::appCache(), standard_paths::cache() / name);
}

GTEST_TEST(standard_paths_test_unix, appRuntime)
{
    const auto name = std::string_view{"app runtime"};
    const auto app = standard_paths::App{name};
    ASSERT_EQ(standard_paths::appRuntime(), standard_paths::runtime() / name);
}

standard_paths::Filesystem::path userDirsPath(std::string_view dir)
{
    auto stream = std::ifstream{standard_paths::config() / "user-dirs.dirs"};
    assert(stream);
    stream.exceptions(std::ios::failbit | std::ios::badbit);
    auto line = std::string{};
    line.reserve(1024);
    while (line.find(dir) == std::string::npos) {
        line.clear();
        while (stream.peek() == '\n')
            stream.get();

        do {
            line += stream.get();
        } while (!stream.eof() && (stream.peek() != '\n'));
    }
    auto begin = std::begin(line) + dir.size() + 1;
    auto end = std::end(line);
    if (*begin == '"') {
        ++begin;
        --end;
    }
    return {begin, end};
}

GTEST_TEST(standard_paths_test_unix, desktop)
{
    ASSERT_EQ(standard_paths::desktop(), userDirsPath("XDG_DESKTOP_DIR"));
}

GTEST_TEST(standard_paths_test_unix, download)
{
    ASSERT_EQ(standard_paths::download(), userDirsPath("XDG_DOWNLOAD_DIR"));
}

GTEST_TEST(standard_paths_test_unix, templates)
{
    ASSERT_EQ(standard_paths::templates(), userDirsPath("XDG_TEMPLATES_DIR"));
}

GTEST_TEST(standard_paths_test_unix, publicshare)
{
    ASSERT_EQ(standard_paths::publicshare(), userDirsPath("XDG_PUBLICSHARE_DIR"));
}

GTEST_TEST(standard_paths_test_unix, documents)
{
    ASSERT_EQ(standard_paths::documents(), userDirsPath("XDG_DOCUMENTS_DIR"));
}

GTEST_TEST(standard_paths_test_unix, music)
{
    ASSERT_EQ(standard_paths::music(), userDirsPath("XDG_MUSIC_DIR"));
}

GTEST_TEST(standard_paths_test_unix, pictures)
{
    ASSERT_EQ(standard_paths::pictures(), userDirsPath("XDG_PICTURES_DIR"));
}

GTEST_TEST(standard_paths_test_unix, videos)
{
    ASSERT_EQ(standard_paths::videos(), userDirsPath("XDG_VIDEOS_DIR"));
}

int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
