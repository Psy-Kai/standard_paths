#include <fstream>

#include <gtest/gtest.h>
#include <standard_paths/standard_paths.h>

GTEST_TEST(standard_paths_test_win, home)
{
    const auto path = standard_paths::home();
    ASSERT_TRUE(path.is_absolute());

    wchar_t *expectedPath = nullptr;
    SHGetKnownFolderPath(FOLDERID_Profile, KF_FLAG_DONT_VERIFY, 0, &expectedPath);
    EXPECT_EQ(path, standard_paths::Filesystem::path{expectedPath});
    CoTaskMemFree(expectedPath);
}

GTEST_TEST(standard_paths_test_win, data)
{
    const auto path = standard_paths::data();
    ASSERT_TRUE(path.is_absolute());

    wchar_t *expectedPath = nullptr;
    SHGetKnownFolderPath(FOLDERID_LocalAppData, KF_FLAG_DONT_VERIFY, 0, &expectedPath);
    EXPECT_EQ(path, standard_paths::Filesystem::path{expectedPath});
    CoTaskMemFree(expectedPath);
}

GTEST_TEST(standard_paths_test_win, config)
{
    const auto path = standard_paths::config();
    ASSERT_TRUE(path.is_absolute());

    wchar_t *expectedPath = nullptr;
    SHGetKnownFolderPath(FOLDERID_LocalAppData, KF_FLAG_DONT_VERIFY, 0, &expectedPath);
    EXPECT_EQ(path, standard_paths::Filesystem::path{expectedPath});
    CoTaskMemFree(expectedPath);
}

GTEST_TEST(standard_paths_test_win, cache)
{
    const auto path = standard_paths::cache();
    ASSERT_TRUE(path.is_absolute());

    wchar_t *expectedPath = nullptr;
    SHGetKnownFolderPath(FOLDERID_LocalAppData, KF_FLAG_DONT_VERIFY, 0, &expectedPath);
    EXPECT_EQ(path, standard_paths::Filesystem::path{expectedPath} / "cache");
    CoTaskMemFree(expectedPath);
}

GTEST_TEST(standard_paths_test_win, runtime_dir)
{
    const auto runtimePath = standard_paths::runtime();
    ASSERT_TRUE(runtimePath.is_absolute());
    ASSERT_EQ(runtimePath, standard_paths::home());
}

GTEST_TEST(standard_paths_test_win, dataDirs)
{
    const auto dataDirs = standard_paths::dataDirs();
    ASSERT_EQ(dataDirs.size(), 1);
    const auto &path = dataDirs.front();
    ASSERT_TRUE(path.is_absolute());

    wchar_t *expectedPath = nullptr;
    SHGetKnownFolderPath(FOLDERID_LocalAppData, KF_FLAG_DONT_VERIFY, 0, &expectedPath);
    EXPECT_EQ(path, standard_paths::Filesystem::path{expectedPath});
    CoTaskMemFree(expectedPath);
}

GTEST_TEST(standard_paths_test_win, configDirs)
{
    const auto configDirs = standard_paths::configDirs();
    ASSERT_EQ(configDirs.size(), 1);
    const auto &path = configDirs.front();
    ASSERT_TRUE(path.is_absolute());

    wchar_t *expectedPath = nullptr;
    SHGetKnownFolderPath(FOLDERID_LocalAppData, KF_FLAG_DONT_VERIFY, 0, &expectedPath);
    EXPECT_EQ(path, standard_paths::Filesystem::path{expectedPath});
    CoTaskMemFree(expectedPath);
}

GTEST_TEST(standard_paths_test_win, app)
{
    const auto name = std::string_view{"some name"};
    const auto app = standard_paths::App{name};
    ASSERT_EQ(standard_paths::App::name(), name);
}

GTEST_TEST(standard_paths_test_win, appData)
{
    const auto name = std::string_view{"app data"};
    const auto app = standard_paths::App{name};
    ASSERT_EQ(standard_paths::appData(), standard_paths::data() / name);
}

GTEST_TEST(standard_paths_test_win, appConfig)
{
    const auto name = std::string_view{"app config"};
    const auto app = standard_paths::App{name};
    ASSERT_EQ(standard_paths::appConfig(), standard_paths::config() / name);
}

GTEST_TEST(standard_paths_test_win, appCache)
{
    const auto name = std::string_view{"app cache"};
    const auto app = standard_paths::App{name};
    ASSERT_EQ(standard_paths::appCache(), standard_paths::cache() / name);
}

GTEST_TEST(standard_paths_test_win, appRuntime)
{
    const auto name = std::string_view{"app runtime"};
    const auto app = standard_paths::App{name};
    ASSERT_EQ(standard_paths::appRuntime(), standard_paths::runtime() / name);
}

GTEST_TEST(standard_paths_test_win, desktop)
{
    const auto path = standard_paths::desktop();
    ASSERT_TRUE(path.is_absolute());

    wchar_t *expectedPath = nullptr;
    SHGetKnownFolderPath(FOLDERID_Desktop, KF_FLAG_DONT_VERIFY, 0, &expectedPath);
    EXPECT_EQ(path, standard_paths::Filesystem::path{expectedPath});
    CoTaskMemFree(expectedPath);
}

GTEST_TEST(standard_paths_test_win, download)
{
    const auto path = standard_paths::download();
    ASSERT_TRUE(path.is_absolute());

    wchar_t *expectedPath = nullptr;
    SHGetKnownFolderPath(FOLDERID_Downloads, KF_FLAG_DONT_VERIFY, 0, &expectedPath);
    EXPECT_EQ(path, standard_paths::Filesystem::path{expectedPath});
    CoTaskMemFree(expectedPath);
}

GTEST_TEST(standard_paths_test_win, templates)
{
    ASSERT_TRUE(standard_paths::templates().empty());
}

GTEST_TEST(standard_paths_test_win, publicshare)
{
    const auto path = standard_paths::publicshare();
    ASSERT_TRUE(path.is_absolute());

    wchar_t *expectedPath = nullptr;
    SHGetKnownFolderPath(FOLDERID_Public, KF_FLAG_DONT_VERIFY, 0, &expectedPath);
    EXPECT_EQ(path, standard_paths::Filesystem::path{expectedPath});
    CoTaskMemFree(expectedPath);
}

GTEST_TEST(standard_paths_test_win, documents)
{
    const auto path = standard_paths::documents();
    ASSERT_TRUE(path.is_absolute());

    wchar_t *expectedPath = nullptr;
    SHGetKnownFolderPath(FOLDERID_Documents, KF_FLAG_DONT_VERIFY, 0, &expectedPath);
    EXPECT_EQ(path, standard_paths::Filesystem::path{expectedPath});
    CoTaskMemFree(expectedPath);
}

GTEST_TEST(standard_paths_test_win, music)
{
    const auto path = standard_paths::music();
    ASSERT_TRUE(path.is_absolute());

    wchar_t *expectedPath = nullptr;
    SHGetKnownFolderPath(FOLDERID_Music, KF_FLAG_DONT_VERIFY, 0, &expectedPath);
    EXPECT_EQ(path, standard_paths::Filesystem::path{expectedPath});
    CoTaskMemFree(expectedPath);
}

GTEST_TEST(standard_paths_test_win, pictures)
{
    const auto path = standard_paths::pictures();
    ASSERT_TRUE(path.is_absolute());

    wchar_t *expectedPath = nullptr;
    SHGetKnownFolderPath(FOLDERID_Pictures, KF_FLAG_DONT_VERIFY, 0, &expectedPath);
    EXPECT_EQ(path, standard_paths::Filesystem::path{expectedPath});
    CoTaskMemFree(expectedPath);
}

GTEST_TEST(standard_paths_test_win, videos)
{
    const auto path = standard_paths::videos();
    ASSERT_TRUE(path.is_absolute());

    wchar_t *expectedPath = nullptr;
    SHGetKnownFolderPath(FOLDERID_Videos, KF_FLAG_DONT_VERIFY, 0, &expectedPath);
    EXPECT_EQ(path, standard_paths::Filesystem::path{expectedPath});
    CoTaskMemFree(expectedPath);
}

int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
